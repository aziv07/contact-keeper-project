import express from 'express'

const router = express.Router()
// getting all users contacts
router.get('/', (req, res) => res.json({ msg: 'contacts...' }))

router.post('/', (req, res) => res.json({ msg: 'adding contact...' }))

router.put('/:id', (req, res) => res.json({ msg: 'updating contact...' }))

router.delete('/:id', (req, res) => res.json({ msg: 'deleting contact...' }))

export default router
