import express from 'express'
import authRoutes from './routes/auth.js'
import usersRoutes from './routes/users.js'
import contactsRoutes from './routes/contacts.js'
import env from 'dotenv'
const app = express()

env.config()

app.get('/api', (req, res) => res.json({ msg: 'welcome to ur endpoint!' }))

app.use('/api/users', usersRoutes)
app.use('/api/auth', authRoutes)
app.use('/api/contacts', contactsRoutes)

const port = process.env.PORT
app.listen(port, () => console.log('server started on PORT :' + port))
